public class GCDLoop{
	public static void main(String[] argv) {
	
		int d = Integer.parseInt(argv[0]);
		int e = Integer.parseInt(argv[1]);
		
		System.out.println(gcd(d,e));
	}

	public static Integer gcd(int c, int b){
		int a = c > b ? c : b;
		b = c > b ? b : c;

		while(a != 0 && b != 0) {
			c = a%b;
			a = b;
			b = c;
		}
		return a > b ? a : b;
	}
}