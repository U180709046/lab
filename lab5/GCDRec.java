public class GCDRec{
	public static void main(String[] argv) {
	
		int d = Integer.parseInt(argv[0]);
		int e = Integer.parseInt(argv[1]);
		
		System.out.println(gcd(d,e));
	}

	public static Integer gcd(int a, int b){
		return (a > 0 && b > 0) ? gcd(b, a%b) : a;
	}
}