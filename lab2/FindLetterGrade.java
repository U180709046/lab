public class FindLetterGrade{
    public static void main(String[] args){
        int grade = Integer.parseInt(args[0]);
        String letter;
        if(100 >= grade && grade >= 90){
            letter = "Your grade is A";
        }else if(90 >= grade && grade >= 80){
            letter = "Your grade is B";
        }
        else if(80 >= grade && grade >= 70){
            letter = "Your grade is C";
        }
        else if(70 >= grade && grade >= 60){
            letter = "Your grade is D";
        }
        else if(60 >= grade && grade >= 0){
            letter = "Your grade is F";
        }
        else{
            letter = "It is not a valid score!";
        }
        
        System.out.println(letter);
    }
}
