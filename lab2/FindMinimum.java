public class FindMinimum{
    public static void main(String[] args){
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);
        int c = Integer.parseInt(args[2]);

        if(a <= b && a <= c){
            System.out.println("Minimum: "+a);
        }else if(b <= a && b <= c){
            System.out.println("Minimum: "+b);
        }else{
            System.out.println("Minimum: "+c);
        }
    }
}
