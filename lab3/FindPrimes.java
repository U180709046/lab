
public class FindPrimes{
    public static void main(String[] args){
           int n = Integer.parseInt(args[0]);
           int i;
		   String result = "";
           for(i = 2; i <= n; i++){
                if(isPrime(i))
                     //System.out.print(i+",");
					if(result != "")
						result = result+","+i;
					else
						result = result+i;
			}
		System.out.println(result);
        }
    public static boolean isPrime(int num){
            for(int m = 2; m < num; m++){
                if(num%m==0)
                    return false;
            }
            return true;           
        }
    }
