import Point;

class Rectangle{
    int sideA, sideB;
    Point topLeft, topRight, botLeft, botRight;

    public Rectangle(Point left, int a, int b){
        this.topLeft = left;
        this.sideA = a;
        this.sideB = b;
        this.topRight = new Point(left.xCoord+sideA,left.yCoord) ;
        this.botLeft = new Point(left.xCoord,left.yCoord-sideB);
        this.botRight = new Point(left.xCoord+sideA,left.yCoord-sideB);
    }

    public int area(){
        return this.sideA*this.sideB;
    }

    public int perimeter(){
        return (this.sideA+this.sideB)*2;
    }

    public Point[] corners(){
        return new Point[] {topLeft,topRight,botLeft,botRight};
    }
}