import Rectangle;
import Point;
import Circle;

class Main{
    public static void main(String[] argv){
        Rectangle rect = new Rectangle(new Point(4,6), 5, 9);
        Circle c = new Circle(new Point(10,10), 10);
        Circle otC = new Circle(new Point(15,15), 10);

        System.out.println("Rectangle=> topLeft:("+rect.topLeft.xCoord+","+rect.topLeft.yCoord+") A Side: "+rect.sideA+" ,B Side: "+rect.sideB);
        System.out.println("Area Rectangle:"+ rect.area());
        System.out.println("Area Rectangle:"+ rect.area());
        System.out.println("Perimeter Rectangle:"+ rect.perimeter());
        System.out.print("Corners Rectangle: ");

        for(int i = 0; i < 4; i++){
            System.out.print("("+rect.corners()[i].xCoord+","+ rect.corners()[i].yCoord+")");
        }
        System.out.print("\n\n");

        System.out.println("Circle=> Center:("+c.center.xCoord+","+c.center.yCoord+") Radius: "+c.radius);
        System.out.println("Other Circle=> Center:("+otC.center.xCoord+","+otC.center.yCoord+") Radius: "+otC.radius);
        System.out.println("Area Circle:"+c.area());
        System.out.println("Perimeter Circle:"+c.perimeter());
        System.out.println("Intersect Circle:"+c.intersect(otC));
    }
}