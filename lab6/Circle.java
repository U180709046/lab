import Point;
import Math;

class Circle{
    Point center;
    int radius;

    public Circle(Point c, int r){
        this.center = c;
        this.radius = r;
    }

    public double area(){
        return Math.PI*Math.pow(this.radius, 2);
    }

    public double perimeter(){
        return Math.PI*this.radius*2;
    }

    public boolean intersect(Circle othCircle){
        return  (radius+othCircle.radius) >= Math.sqrt(Math.pow(center.xCoord - othCircle.center.xCoord, 2) + Math.pow(center.yCoord - othCircle.center.yCoord, 2)) ? true : false;
    }
}