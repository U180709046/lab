import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };
        
        int player = 0;
        int move = 0;
		printBoard(board);
        boolean win = false;

        while(move < 9 && !win){
            int row,col;
		    do{
            System.out.print("Player "+(player+1)+" enter row number:");
		    row = reader.nextInt();
		    System.out.print("Player "+(player+1)+" enter column number:");
		    col = reader.nextInt();
		    }while(!(row >0 && row < 4 && col > 0 && col < 4 && board[row-1][col-1] == ' '));

            if(player == 0)
                board[row - 1][col - 1]='X';
            else
                board[row - 1][col - 1]='O';
            
            player = (player+1)%2;
            printBoard(board);
            move++;
            win = checkBoard(board,row,col);
        }
        if(win)
            System.out.println(player != 0 ? "Winner is Player 1 with X" : "Winner is Player 2 with O");
		else
			System.out.println("Games ended with a draw.");
		reader.close();
	}
     public static boolean checkBoard(char[][] board,int row,int col){
        char tic = board[row - 1][col - 1];
		boolean[] winWays =  {true,true,true,true}; //Array for possible winning ways.
		int i = 0;
		int cross = 2;
		while(i<3 && (winWays[0] || winWays[1] || winWays[2] || winWays[3])){
			if(winWays[0]) winWays[0] = winWays[0] && board[i][col - 1]==tic;//horizontal
			if(winWays[1]) winWays[1] = winWays[1] && board[row-1][i]==tic;//vertical
			if(winWays[2]) winWays[2] = winWays[2] && board[i][i] == tic;//diagonal left
			if(winWays[3]) winWays[3] = winWays[3] && board[i][cross] == tic;//diagonal right
			i++;
			cross--;
		}
		return winWays[0] || winWays[1] || winWays[2] || winWays[3];
		
    }
	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}

	/*
	My first try
    public static boolean checkBoardArt(char[][] board,int row,int col){
        char tic = board[row - 1][col - 1]; 
        if(board[0][col - 1]==tic && board[1][col - 1]==tic && board[2][col - 1]==tic)
            return true;
        if(board[row -1][0]==tic && board[row -1][1]==tic && board[row -1][2]==tic)
            return true;
        if((row != 1 || col != 1) || (row == 1 && col == 1)){
            if(board[0][0]==tic && board[1][1]==tic && board[2][2]==tic)
                return true;
            if(board[2][0]==tic && board[1][1]==tic && board[0][2]==tic)
                return true;
        }   
        return false;             
    }*/
    
}
